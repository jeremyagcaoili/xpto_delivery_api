<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    public function routes()
    {
        return $this->hasMany('App\PointConnections', 'point_from', 'id');
    }
}
