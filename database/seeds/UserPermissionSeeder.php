<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_permissions')->insert([
            [
                'name' => 'admin',
                'description' => 'Admins Access',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'common',
                'description' => 'Can use api',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
