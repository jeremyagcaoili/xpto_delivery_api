<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResource;
use App\Http\Resources\PointConnectionResource;
use App\Http\Resources\SuccessResource;
use App\PointConnections;
use App\Points;
use Exception;
use Illuminate\Http\Request;

class PointConnectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arPointConnections = PointConnections::all();

        return PointConnectionResource::collection($arPointConnections);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $validateInput = $request->validate([
                "point_from" => "required|integer",
                "point_to" => "required|integer",
                "travel_time" => "required",
                "travel_cost" => "required"
            ]);

            if($request->isMethod('put'))
            {
                $validateInput = $request->validate([
                    "id" => "required"
                ]);

                $objPointConnection = PointConnections::find($request->id);
                if(!$objPointConnection)
                {
                    throw new Exception("Can't find a route with that id.");
                }
            }
            else
            {
                $validateInput = $request->validate([
                    "bi_directional" => "required|boolean"
                ]);

                $objPointConnection = PointConnections::where('point_from', $request->point_from)
                    ->where('point_to', $request->point_to)
                    ->first();
                if(!$objPointConnection)
                {
                    $objPointConnection = new PointConnections();
                }

                
                $bkPointConnectionB = PointConnections::where('point_from', $request->point_to)
                    ->where('point_to', $request->point_from)
                    ->first();
                if(!$bkPointConnectionB)
                {
                    $bkPointConnectionB = new PointConnections();
                }
            }

            // Check if points exists
            $objPointFrom = Points::find($request->point_from);
            $objPointTo = Points::find($request->point_to);

            if(!$objPointFrom && !$objPointTo)
            {
                throw new Exception("Can't find point_from / point_to with that id.");
            }

            if(isset($bkPointConnectionB))
            {
                $bkPointConnectionB->point_from = $request->point_to;
                $bkPointConnectionB->point_to = $request->point_from;
                $bkPointConnectionB->travel_time = $request->travel_time;
                $bkPointConnectionB->travel_cost = $request->travel_cost;

                $bkPointConnectionB->saveOrFail();
            }

            $objPointConnection->point_from = $request->point_from;
            $objPointConnection->point_to = $request->point_to;
            $objPointConnection->travel_time = $request->travel_time;
            $objPointConnection->travel_cost = $request->travel_cost;

            if($objPointConnection->save())
            {
                return new PointConnectionResource($objPointConnection);
            }
            throw new Exception("Failed to save");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try
        {
            $validateInput = $request->validate([
                "id" => "required|integer"
            ]);

            $objPointConnection = PointConnections::find($validateInput["id"]);
            if($objPointConnection)
            {
                return new PointConnectionResource($objPointConnection);
            }
            throw new Exception("Can't find route with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $validateInput = $request->validate([
                "id" => "required|integer"
            ]);

            $objPointConnection = PointConnections::find($validateInput["id"]);
            if($objPointConnection)
            {
                $objPointConnection->delete();
                return new SuccessResource($objPointConnection);
            }
            throw new Exception("Can't find route with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }
}