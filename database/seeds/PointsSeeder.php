<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $letters = range('A', 'I');
        
        $arInsert = [];
        foreach($letters as $letter)
        {
            $arInsert[] = [
                'point_name' => $letter,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        DB::table('points')->insert(
            $arInsert
        );
    }
}
