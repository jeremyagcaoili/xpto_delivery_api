<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::tokensCan([
            'points-create' => 'can create points',
            'points-read' => 'can read points',
            'points-update' => 'can edit points',
            'points-delete' => 'can delete points',
            'point_connections-create' => 'can create points',
            'point_connections-read' => 'can read points',
            'point_connections-update' => 'can edit points',
            'point_connections-delete' => 'can delete points',
            'shortest_path-read' => 'can use shortest path',
            'users-create' => 'can create user',
            'users-read' => 'can read user',
            'users-update' => 'can update user',
            'users-delete' => 'can delete users',
            'user_permissions-read' => 'can read permissions'
        ]);
        Passport::routes();
        //
    }
}
