<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PointsSeeder::class);
        $this->call(PointConnectionsSeeder::class);
        $this->call(UserPermissionSeeder::class);
        $this->call(PermissionPriviledgesSeeder::class);
        $this->call(UserSeeder::class);
    }
}
