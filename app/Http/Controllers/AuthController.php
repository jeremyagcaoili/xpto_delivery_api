<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserPermission;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required',
            'password' => 'required|confirmed',
            'permission_id' => 'required|integer'
        ]);

        $validateData['password'] = bcrypt($request->password);
        $validateData['permission_id'] = intval($validateData['permission_id']);

        // // Get User Permission
        $userPermission = UserPermission::find($validateData['permission_id']);
        if($userPermission)
        {
            $user = User::create($validateData);
            
            $arPriviledges = $userPermission->getScopes();
            $token = $user->createToken('My Token', $arPriviledges)->accessToken;

            return response([
                "user" => $user,
                "token" => $token
            ]);
        }

        return response([
            "success" => false,
            "message" => "Can't find user_permissions with that id."
        ]);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password'=> 'required'
        ]);

        if(!auth()->attempt($loginData))
        {
            return response([
                'success' => false,
                'message' => 'Invalid credentials'
            ]);
        }

        $user = Auth::user();
        $userPermission = UserPermission::find($user->permission_id);
        $arPriviledges = $userPermission->getScopes();

        $token = $user->createToken('My Token', $arPriviledges)->accessToken;
        return response([
            "user" => $user,
            "token" => $token
        ]);
    }
}
