<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionPriviledges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_priviledges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_permission_id')->unsigned();
            $table->foreign('user_permission_id')->references('id')->on('user_permissions')->onDelete('cascade');
            $table->string('module_name');
            $table->boolean('create');
            $table->boolean('read');
            $table->boolean('update');
            $table->boolean('delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_priviledges');
    }
}
