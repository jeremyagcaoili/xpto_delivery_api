<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResource;
use App\Http\Resources\PointResources;
use App\Http\Resources\SuccessResource;
use App\Points;
use Exception;
use Illuminate\Http\Request;

class PointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
{
        $objPoints = Points::all();

        return PointResources::collection($objPoints);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            if($request->isMethod('put'))
            {
                $validatedInput = $request->validate([
                    'id' => 'required'
                ]);

                $objPoint = Points::find($validatedInput["id"]);
                if(!$objPoint)
                {
                    throw new \Exception("Can't find point with that id.");
                }
            }
            else
            {
                $validatedInput = $request->validate([
                    "point_name" => "required"
                ]);

                $objPoint = new Points();
            }

            $objPoint->point_name = $request->point_name;

            if($objPoint->save())
            {
                return new PointResources($objPoint);
            }
            
            throw new \Exception("Saving failed.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }

    public function show(Request $request)
    {
        try
        {
            $validatedInput = $request->validate([
                'get' => 'required',
                'search_by' => 'required|in:ById,ByPointName'
            ]);

            $arSearchBy = ["ById" => "id", "ByPointName" => "point_name"];

            $objPoint = Points::where($arSearchBy[$validatedInput["search_by"]], $validatedInput["get"])->first();
            if($objPoint)
            {
                return new PointResources($objPoint);
            }
            throw new Exception("Can't find that point.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $validatedInput = $request->validate([
                "id" => "required|integer"
            ]);

            $objPoint = Points::find($validatedInput["id"]);
            if($objPoint)
            {
                $objPoint->delete();
                return new SuccessResource(null);
            }
            
            throw new Exception("Can't find point with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }
}