<?php

namespace App\Http\Resources;

use App\PointConnections;
use App\Points;
use Illuminate\Http\Resources\Json\JsonResource;

class ShortestPathResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $returnResult = [];
        $overAllCost = 0;
        $overAllTime = 0;

        $arResult = $this->arResult;
        $route_no = 0;
        if(!is_null($arResult))
        {
            foreach($arResult as $rowResult)
            {
                $objPoint = Points::find($rowResult["point_id"]);
                $objPointConnection = $rowResult["route_id"] ?: new PointConnectionResource(PointConnections::find($rowResult["route_id"]));
                $returnResult[] = [
                    "step" => $route_no++,
                    "route_id" => $objPointConnection,
                    "travel_time" => $rowResult["shortest_path_value"]["travel_time"],
                    "travel_cost" => $rowResult["shortest_path_value"]["travel_cost"],
                    "point" => new PointResources($objPoint),
                ];

                $overAllCost = $rowResult["shortest_path_value"]["travel_cost"];
                $overAllTime = $rowResult["shortest_path_value"]["travel_time"];
            }

            return [
                "success" => true,
                "routes" => $returnResult,
                "total_travel_time" => $overAllTime,
                "total_travel_cost" => $overAllCost
            ];
        }

        return [
            "success" => false,
            "routes" => null,
            "message" => "There's no path connected for the Points"
        ]; 
    }

    public function with($request)
    {
        return ['code' => 200];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(200, 'OK');
    }
}