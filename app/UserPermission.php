<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Passport;

class UserPermission extends Model
{
    protected $table = "user_permissions";

    public function privileges()
    {
        return $this->hasMany('App\PermissionPriviledges');
    }

    public function getScopes()
    {
        $arMyScopes = [];

        $arCRUD = ["create", "read", "update", "delete"];
        $arPassportScopes = Passport::scopes();
        foreach($this->privileges as $privilege)
        {
            $strModuleName = $privilege->module_name;
            foreach($arCRUD as $a)
            {
                if($privilege->$a)
                {
                    $arMyScopes[] = $strModuleName . "-" . $a;
                }
            }
        }
        
        return $arMyScopes;
    }
}
