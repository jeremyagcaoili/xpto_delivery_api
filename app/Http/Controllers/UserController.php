<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResource;
use App\Http\Resources\SuccessResource;
use App\Http\Resources\UserResource;
use App\User;
use App\UserPermission;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objPoints = User::all();

        return UserResource::collection($objPoints);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validateData = $request->validate([
                'id' => 'required',
                'name' => 'required|max:55',
                'email' => 'email|required',
                'password' => 'required|confirmed',
                'permission_id' => 'required|integer'
            ]);
    
            $validateData['password'] = bcrypt($request->password);
            $validateData['permission_id'] = intval($validateData['permission_id']);
    
            // // Get User Permission
            $user = User::find($validateData["id"]);
            $userPermission = UserPermission::find($validateData['permission_id']);
            if($userPermission && $user)
            {
                $user->name = $validateData["name"];
                $user->email = $validateData["email"];
                $user->password = $validateData["password"];
                $user->permission_id = $validateData["permission_id"];
                
                $user->save();
                
                return new UserResource($user);
            }
            throw new Exception("Can't find user / user permission with that id");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $validateData = $request->validate([
                'id' => 'required|integer'
            ]);

            $user = User::find($validateData["id"]);
            if($user)
            {
                return new UserResource($user);
            }
            throw new \Exception("Can't find user with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $validateData = $request->validate([
                "id" => "required|integer"
            ]);

            $user = User::find($validateData["id"]);
            if($user)
            {
                $user->delete();
                return new SuccessResource(null);
            }
            
            throw new Exception("Can't find point with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }
}
