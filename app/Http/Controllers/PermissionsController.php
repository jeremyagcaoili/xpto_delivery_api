<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResource;
use App\Http\Resources\PermissionResources;
use App\UserPermission;
use Exception;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    //
    public function index()
    {
        $permissions = UserPermission::all();

        return PermissionResources::collection($permissions);
    }

    public function show(Request $request)
    {
        try
        {
            $validateInput = $request->validate([
                "id" => "required"
            ]);

            $permission = UserPermission::find($request->id);
            if($permission)
            {
                return new PermissionResources($permission);
            }
            throw new Exception("Can't find user_permission with that id");
        }
        catch(Exception $e)
        {
            return new FailedResource($e);
        }
    }
}
