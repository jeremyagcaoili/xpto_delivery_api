<?php

namespace App\Http\Controllers;

use App\CustomLibrary\Dijkstra;
use App\PointConnections;
use App\CustomLibrary\Graph;
use App\Http\Resources\FailedResource;
use App\Http\Resources\ShortestPathResource;
use App\Points;
use Illuminate\Http\Request;

class ShortestPathController extends Controller
{
    public function find(Request $request)
    {
        try
        {
            $validateData = $request->validate([
                'point_from' => 'required',
                'point_to' => 'required',
                'search_by' => 'required|in:ById,ByPointName'
            ]);

            // Check points
            $arSearchBy = ["ById" => "id", "ByPointName" => "point_name"];
            $objPointFrom = Points::where($arSearchBy[$validateData["search_by"]], $validateData["point_from"])->first();
            $objPointTo = Points::where($arSearchBy[$validateData["search_by"]], $validateData["point_to"])->first();

            if(!$objPointTo && !$objPointFrom)
            {
                throw new \Exception("Can't find provided Points.");
            }

            // Prepare the columns
            $arColumns = [];
            $arPoints = Points::all();
            foreach($arPoints as $objPoint)
            {
                $arColumns[] = $objPoint->id;
            }

            // Create a Graph
            $objGraph = new Graph($arColumns);

            // Get all routes
            $arRoutes = PointConnections::all();
            foreach($arRoutes as $objRoute)
            {
                $objGraph->addVertex(
                    $objRoute->point_from, 
                    $objRoute->point_to,
                    $objRoute->id,
                    $objRoute->travel_time,
                    $objRoute->travel_cost,
                    false
                );
            }

            $objDijsktra = new Dijkstra($objGraph);
            $objResult = $objDijsktra->findShortestPath($objPointFrom->id, $objPointTo->id);
            return new ShortestPathResource($objResult);
        }
        catch(\Exception $e)
        {
            return new FailedResource($e);
        }
    }
}