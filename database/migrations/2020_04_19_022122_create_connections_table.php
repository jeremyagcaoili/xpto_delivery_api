<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_connections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('point_from')->unsigned();
            $table->foreign('point_from')->references('id')->on('points')->onDelete('cascade');
            $table->integer('point_to')->unsigned();
            $table->foreign('point_to')->references('id')->on('points')->onDelete('cascade');
            $table->float('travel_time');
            $table->float('travel_cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connections');
    }
}
