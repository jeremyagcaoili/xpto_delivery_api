<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $permission = $this->permission;
        $permission->privileges = $this->permission->privileges;
        return [
            "success" => true,
            "id" => $this->id,
            "email" => $this->email,
            "permission" => $this->permission
        ];
    }

    public function with($request)
    {
        return ['code' => 200];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(200, 'OK');
    }
}
