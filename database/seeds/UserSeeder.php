<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('secret'),
                'permission_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Commoner 1',
                'email' => 'commoner1@mail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('secret'),
                'permission_id' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
