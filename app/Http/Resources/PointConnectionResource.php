<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PointConnectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'point_from' => $this->point_from,
            'point_to' => $this->point_to,
            'travel_time' => $this->travel_time,
            'travel_cost' => $this->travel_cost
        ];
    }
}