# XPTO Delivery API
## Getting Started
This API is written in PHP using the Laravel Framework. The algorithm use for finding the shortest path is Dijskra algorithm on a Graph data structure.

Here is the sample data. Note that the arrows for routes are not **Bi-Directional**. This means that going from H -> A is impossible. To make it Bi-Directional, you can add a route.
![Sample data](https://i.imgur.com/5QUykkB.png)

This repository includes the migrations for the database, authentications and seeders for sample data. To set it up locally, run the following commands.
```
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan passport:install
```
Make sure that before you run everything for your local setup, a database is already created. You can run the following command to create your own database.:
```sql
CREATE SCHEMA `xpto_delivery_db` DEFAULT CHARACTER SET utf8 ;
```
Also update your .env file to connect your database. (You can copy the .env.example inside the repository.

Here are the sample users that you can use for this API.

|email|password|permission|
|-----|--------|----------|
|admin@mail.com|secret|Admin|
|commoner1@mail.com|secret|Common|

## Authentication
This API uses HTTP Bearer authentication.
Each request to the API must have a header called Authorization and must contain the token like the example below.
```
Authorization: Bearer QWxhZGRpbjpPcGVuU2VzYW1l
```

You must also send the Accept key in the header as `application/json`.

In over all, this should be your header:
```
Authorization: Bearer [INSERT TOKEN HERE]
Accept: application/json
```

### Getting your token.
To get your token, you must login to `/api/login`. If you are successfully authenticated, it will return the token that you need.
#### URL `POST`
```
/api/login
```
#### URL Params
`email=[email|string|required]` - user's email address.

`password=[required]` - user's password.

#### Success Response
|Attribute|Type|Description|
|-|-|-|
|User|Object|Contains informations about the user.|
|token|string|The token for authentication.|
***Actual Result***
```
{
    "user": {
        "id": 1,
        "name": "Admin",
        "email": "admin@mail.com"
    },
    "token": "sdfasdfasdf"
}
```
    
## Permissions
Currently, there are two types of user for this API. **Admin** and **Common** permissions. Each permission type has certain priviledges. The table below shows the priviledges of each type.
### Admin Priviledges:
|Module|Create|Read|Update|Delete|
|------|------|----|------|------|
|Find|N/A|True|N/A|N/A|
|Points|True|True|True|True|
|Routes|True|True|True|True|
|Users|True|True|True|True|
|User Permissions|N/A|True|N/A|N/A|

### Common Priviledges:
|Module|Create|Read|Update|Delete|
|------|------|----|------|------|
|Find|N/A|True|N/A|N/A|
|Points|False|True|False|False|
|Routes|False|True|False|False|
|Users|False|False|False|False|
|User Permissions|N/A|False|N/A|N/A|

## Shortest Path
To find the shortest path with the given points, you must send a request to `/api/path`.
#### URL `GET`
```
/api/path
```
#### URL Params
`point_from=[required]` -> If `seach_by` is `ById`, pass the id. If `ByPointName`, pass the point name. 

`point_to=[required]` -> If `seach_by` is `ById`, pass the id. If `ByPointName`, pass the point name. 

`search_by=[required|string]` - You must choose if search by `ById` or `ByPointName`.

*Note: If `ByPointName` is used and there are two or more points with the same name, it will return the first value. It is recommended that you use the `ById` to avoid this issue.*

#### Success Response
|Attribute|Type|Description|
|-|-|-|
|success|boolean|Determines if request is success.|
|routes|Array|List of points to get from point A to point B.|
|step|integer|Step number. (Step 0 means the beginning)
|route_id|integer|The route id that connects between the current point to the next Point.|
|travel_time|float|The time it will take to travel between the points|
|travel_cost|float|The cost it will take to travel between the points|
|point|object|Contains informations about the point.|
|total_travel_time|float|The overall total time to travel between the given points|
|total_travel_cost|float|The overall total cost to travel between the given points|
|code|integer|Response code.|

***Actual Result***
```
"data": {
        "success": true,
        "routes": [
            {
                "step": 0,
                "route_id": null,
                "travel_time": 0,
                "travel_cost": 0,
                "point": {
                    "id": 1,
                    "point_name": "A",
                    "point_connections": [
                        {
                            "id": 1,
                            "point_from": 1,
                            "point_to": 3,
                            "travel_time": 1,
                            "travel_cost": 20
                        },
                        {
                            "id": 2,
                            "point_from": 1,
                            "point_to": 5,
                            "travel_time": 30,
                            "travel_cost": 5
                        },
                        {
                            "id": 3,
                            "point_from": 1,
                            "point_to": 8,
                            "travel_time": 10,
                            "travel_cost": 1
                        }
                    ]
                }
            },
            {
                "step": 1,
                "route_id": 1,
                "travel_time": 1,
                "travel_cost": 20,
                "point": {
                    "id": 3,
                    "point_name": "C",
                    "point_connections": [
                        {
                            "id": 11,
                            "point_from": 3,
                            "point_to": 2,
                            "travel_time": 1,
                            "travel_cost": 12
                        }
                    ]
                }
            },
            {
                "step": 2,
                "route_id": 11,
                "travel_time": 2,
                "travel_cost": 32,
                "point": {
                    "id": 2,
                    "point_name": "B",
                    "point_connections": []
                }
            }
        ],
        "total_travel_time": 2,
        "total_travel_cost": 32
    },
    "code": 200
```
## Points
* #### Get all Points.
    To get all the points, you must send a `GET` request at `/api/points`
    ##### URL `GET`
    ```
    /api/points
    ```
    ##### URL Params
    N/A


* #### Get specific Point.
    To get the information of specific Point, you must send a `GET` request at `/api/point`. You can also specify is you are requesting `ById` or `ByPointName`. Note that if you search by Point Name, if there are two or more Points with the same name, it will return the first value. It is recommended that you search by Id.
    
    ##### URL `GET`
    ```
    /api/point
    ```
    ##### URL Params
    `get=[required]` -> If `seach_by` is `ById`, pass the id. If `ByPointName`, pass the point name. 

    `search_by=[required|string]` - You must choose if search by `ById` or `ByPointName`.


* #### Create a Point
    To create a new Point, send a `POST` request at `/api/point`.
    
    ##### URL `POST`
    ```
    /api/point
    ```
    ##### URL Params
    `point_name=[string|required]` -> The name of the Point.


* #### Update a Point
    To update, send a `PUT` request at `/api/point`.
    
    ##### URL `PUT`
    ```
    /api/point
    ```
    ##### URL Params
    `id`=[integer|required] -> The id of the Point.
    
    `point_name`=[string|required] -> The name of the Point.


* #### Delete a Point
    To delete, send a `DELETE` request at `/api/point`.
    
    ##### URL `DELETE`
    ```
    /api/point
    ```
    
    ##### URL Params
    `id`=[integer|required] -> The id of the Point.

## Routes
* #### Get all Routes.
    To get all the points, you must send a `GET` request at `/api/routes`
    
    ##### URL `GET`
    ```
    /api/routes
    ```
    
    ##### URL Params
    N/A


* #### Get specific Route.
    To get the information of specific Route, you must send a `GET` request at `/api/route`.
    
    ##### URL `GET`
    ```
    /api/route
    ```
    
    ##### URL Params
    `id=[integer|required]` -> The id of the route.


* #### Create a Route
    To create a new Route, send a `POST` request at `/api/route`. Note that if you tried to add an existing route, it will update that route instead. There can be only one or two routes (bi-directional) for each Point connections.
    
    ##### URL `POST`
    ```
    /api/route
    ```
    
    ##### URL Params
    `point_from=[integer|required]` -> The id of the Point A.
    
    `point_to=[integer|required]` -> The id of the Point B.
    
    `travel_time=[float|required]` -> The time it will cost to travel.
    
    `travel_cost=[float|required]` -> The cost of the travel.
    
    `bi_directional=[boolean|required]` -> Determines if route is bi-directional. (1 | 0)


* #### Update a route
    To update, send a `PUT` request at `/api/route`.
    
    ##### URL `PUT`
    ```
    /api/route
    ```
    
    ##### URL Params
    `id`=[integer|required] -> The id of the Route.
    
    `point_from=[integer|required]` -> The id of the Point A.
    
    `point_to=[integer|required]` -> The id of the Point B.
    
    `travel_time=[float|required]` -> The time it will cost to travel.
    
    `travel_cost=[float|required]` -> The cost of the travel.


* #### Delete a Point
    To delete, send a `DELETE` request at `/api/route`.
    
    ##### URL `DELETE`
    ```
    /api/route
    ```
    
    ##### URL Params
    `id`=[integer|required] -> The id of the Route.
    
## Users
* #### Get all Users.
    To get all the User, you must send a `GET` request at `/api/users`
    
    ##### URL `GET`
    ```
    /api/users
    ```
    
    ##### URL Params
    N/A


* #### Get specific User.
    To get the information of specific User, you must send a `GET` request at `/api/user`.
    
    ##### URL `GET`
    ```
    /api/user
    ```
    
    ##### URL Params
    `id=[integer|required]` -> The id of the User.


* #### Register a User
    To create a new User, send a `POST` request at `/api/register`.
    
    ##### URL `POST`
    ```
    /api/register
    ```
    
    ##### URL Params
    `name=[string|required]` -> The name of the user.
    
    `email=[email|required]` -> The email address.
    
    `password=[string|required]` -> Password.
    
    `password_confirmation=[string|required]` -> Confirm Password.
    
    `permission_id=[integer|required]` -> The permission id of the user.


* #### Update a user
    To update, send a `PUT` request at `/api/user`.
    
    ##### URL `PUT`
    ```
    /api/user
    ```
    
    ##### URL Params
    `id=[integer|required]` -> The id of the user.
    
    `name=[string|required]` -> The name of the user.
    
    `email=[email|required]` -> The email address.
    
    `password=[string|required]` -> Password.
    
    `password_confirmation=[string|required]` -> Confirm Password.
    
    `permission_id=[integer|required]` -> The permission id of the user.


* #### Delete a User
    To delete, send a `DELETE` request at `/api/user`.
    
    ##### URL `DELETE`
    ```
    /api/user
    ```
    
    ##### URL Params
    `id`=[integer|required] -> The id of the User.

## User Permissions
* #### Get all Permissions.
    To get all the Permissions, you must send a `GET` request at `/api/permissions`
    
    ##### URL `GET`
    ```
    /api/permissions
    ```

    ##### URL Params
    ```
    N/A
    ``` 

* #### Get specific Permission.
    To get the information of specific Permission, you must send a `GET` request at `/api/permission`.
    
    ##### URL `GET`
    ```
    /api/permission
    ```
    
    ##### URL Params
    `id=[integer|required]` -> The id of the Permission.

