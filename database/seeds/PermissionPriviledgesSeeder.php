<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionPriviledgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_priviledges')->insert([
            [
                'user_permission_id' => 1,
                'module_name' => 'points',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 1,
                'module_name' => 'point_connections',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 1,
                'module_name' => 'shortest_path',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 2,
                'module_name' => 'shortest_path',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 2,
                'module_name' => 'points',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 2,
                'module_name' => 'point_connections',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 1,
                'module_name' => 'users',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_permission_id' => 1,
                'module_name' => 'user_permissions',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
