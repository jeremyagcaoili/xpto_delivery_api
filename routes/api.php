<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/****************************************************
 *              Authentication Routes
 ****************************************************/
Route::post('register', 'AuthController@register')->middleware(['auth:api', 'scope:users-create']);

Route::post('login', 'AuthController@login');

/****************************************************
 *          Users 
 ****************************************************/
Route::get('users', 'UserController@index')->middleware(['auth:api', 'scope:users-read']);

Route::get('user', 'UserController@show')->middleware(['auth:api', 'scope:users-read']);

Route::put('user', 'UserController@store')->middleware(['auth:api', 'scope:users-update']);

Route::delete('user', 'UserController@destroy')->middleware(['auth:api', 'scope:users-delete']);

/*****************************************************
 *                  Points Route
 ****************************************************/
// Get ALL
Route::get('points', 'PointsController@index')->middleware(['auth:api', 'scope:points-read']);

// Get ID
Route::get('point', 'PointsController@show')->middleware(['auth:api', 'scope:points-read']);

// Save
Route::post('point', 'PointsController@store')->middleware(['auth:api', 'scope:points-create']);

// Update
Route::put('point', 'PointsController@store')->middleware(['auth:api', 'scope:points-update']);;

// Delete
Route::delete('point', 'PointsController@destroy')->middleware(['auth:api', 'scope:points-delete']);;


/****************************************************
 *            Point Connection Routes
 ****************************************************/

// Get
Route::get('route', 'PointConnectionsController@show')->middleware(['auth:api', 'scope:point_connections-read']);;
Route::get('routes', 'PointConnectionsController@index')->middleware(['auth:api', 'scope:point_connections-read']);;

// Save
Route::post('route', 'PointConnectionsController@store')->middleware(['auth:api', 'scope:point_connections-create']);;

// Update
Route::put('route', 'PointConnectionsController@store')->middleware(['auth:api', 'scope:point_connections-update']);;

// Delete
Route::delete('route', 'PointConnectionsController@destroy')->middleware(['auth:api', 'scope:point_connections-delete']);;

/******************************************************
 *         Shortest Path Routes
 ******************************************************/
// Find ShortestPath
Route::get('path', 'ShortestPathController@find')->middleware(['auth:api', 'scope:shortest_path-read']);

/******************************************************
 *       Permissions
 ******************************************************/
Route::get('permissions', 'PermissionsController@index')->middleware(['auth:api', 'scope:user_permissions-read']);
Route::get('permission', 'PermissionsController@show')->middleware(['auth:api', 'scope:user_permissions-read']);
