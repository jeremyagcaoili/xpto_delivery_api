<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Points;

class PointConnectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $points = [
            [
                'point_from' => 'A',
                'point_to' => 'C',
                'travel_time' => 1,
                'travel_cost' => 20
            ],
            [
                'point_from' => 'A',
                'point_to' => 'E',
                'travel_time' => 30,
                'travel_cost' => 5
            ],
            [
                'point_from' => 'A',
                'point_to' => 'H',
                'travel_time' => 10,
                'travel_cost' => 1
            ],
            [
                'point_from' => 'H',
                'point_to' => 'E',
                'travel_time' => 30,
                'travel_cost' => 1
            ],
            [
                'point_from' => 'E',
                'point_to' => 'D',
                'travel_time' => 3,
                'travel_cost' => 5
            ],
            [
                'point_from' => 'D',
                'point_to' => 'F',
                'travel_time' => 4,
                'travel_cost' => 50
            ],
            [
                'point_from' => 'F',
                'point_to' => 'I',
                'travel_time' => 45,
                'travel_cost' => 50
            ],
            [
                'point_from' => 'I',
                'point_to' => 'B',
                'travel_time' => 65,
                'travel_cost' => 5
            ],
            [
                'point_from' => 'F',
                'point_to' => 'G',
                'travel_time' => 40,
                'travel_cost' => 50
            ],
            [
                'point_from' => 'G',
                'point_to' => 'B',
                'travel_time' => 64,
                'travel_cost' => 73
            ],
            [
                'point_from' => 'C',
                'point_to' => 'B',
                'travel_time' => 1,
                'travel_cost' => 12
            ]
        ];

        foreach($points as $point){
            $pointFrom = Points::where('point_name', $point['point_from'])->first();
            $pointTo = Points::where('point_name', $point['point_to'])->first();

            DB::table('point_connections')->insert(
                [
                    'point_from' => $pointFrom->id,
                    'point_to' => $pointTo->id,
                    'travel_time' => $point['travel_time'],
                    'travel_cost' => $point['travel_cost'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            );
        }
    }
}
